FROM python:3.11-bullseye

ENV LANG C.UTF-8

RUN apt-get update -qq &&   \
    apt-get install -y -qq  \
    libboost-all-dev        \
    ffmpeg                  \
    libsm6                  \
    libxext6                \
    cmake
